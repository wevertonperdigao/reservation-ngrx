import {Todo} from './todo/model/todo.model';

import * as filterActions from './filter/filter.action';
import {ActionReducerMap} from '@ngrx/store';
import * as fromTodo from './todo/todo.reducer.js';
import * as fromFilter from './filter/filter.reducer';

export interface AppState {

  todos: Todo[];
  filter: filterActions.FilterValids;
}


export const appReducers: ActionReducerMap<AppState> = {
  todos: fromTodo.todoReducer,
  filter: fromFilter.filterReducer
};


