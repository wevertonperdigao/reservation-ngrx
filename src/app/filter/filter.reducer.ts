import * as fromFilterActions from './filter.action';


export const valueInitial: fromFilterActions.FilterValids = 'TODOS';


export function filterReducer(stage = valueInitial, action: fromFilterActions.Actions) {


  switch (action.type) {
    case fromFilterActions.SET_FILTER:
      return action.filter;

    default:
      return stage;
  }

}
