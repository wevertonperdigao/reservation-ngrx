export const SET_FILTER = '[FILTER] Set filter';

export type FilterValids = 'COMPLETADOS' | 'PENDENTES' | 'TODOS';


export class FilterAction {
  readonly type = SET_FILTER;


  constructor(public filter: FilterValids) {
  }

}


export type Actions = FilterAction;
