import {Pipe, PipeTransform} from '@angular/core';
import {Todo} from '../todo/model/todo.model';

import * as fromFilter from './filter.action';

@Pipe({
  name: 'filterTodo'
})
export class FilterTodoPipe implements PipeTransform {

  transform(todos: Todo[], filter: fromFilter.FilterValids): Todo[] {
    switch (filter) {

      case 'COMPLETADOS':
        return todos.filter(todo => todo.completado);

      case 'PENDENTES':
        return todos.filter(todo => !todo.completado);

      default:
        return todos;
    }
  }

}
