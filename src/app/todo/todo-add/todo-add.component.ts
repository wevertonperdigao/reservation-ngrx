import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.reducers';
import {AgreagarTodoAction} from '../todo.actions';

@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styleUrls: ['./todo-add.component.scss']
})
export class TodoAddComponent implements OnInit {


  txtInput: FormControl;

  constructor(private  store: Store<AppState>) {
  }

  ngOnInit() {

    this.txtInput = new FormControl('', Validators.required);
  }

  agregarTodo() {
    if (this.txtInput.valid) {
      this.store.dispatch(new AgreagarTodoAction(this.txtInput.value));
      this.txtInput.setValue('');
    }
  }

}
