import {Component, OnInit} from '@angular/core';

import * as formFilterAction from '../../filter/filter.action';
import {AppState} from '../../app.reducers';
import {select, Store} from '@ngrx/store';
import {FilterAction} from '../../filter/filter.action';
import {Todo} from '../model/todo.model';
import {RemoveAllCompletedTodoAction} from '../todo.actions';

@Component({
  selector: 'app-todo-footer',
  templateUrl: './todo-footer.component.html',
  styleUrls: ['./todo-footer.component.scss']
})
export class TodoFooterComponent implements OnInit {


  filterActive: formFilterAction.FilterValids;
  filtros: formFilterAction.FilterValids[] = ['COMPLETADOS', 'PENDENTES', 'TODOS'];
  pendentes = 0;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.store.pipe()
      .subscribe(state => {
        this.filterActive = state.filter;
        this.countPedentes(state.todos);

      });
  }

  setFilter(filtro: formFilterAction.FilterValids) {
    this.store.dispatch(new FilterAction(filtro));
  }

  countPedentes(todos: Todo[]) {
    this.pendentes = todos.filter(todo => !todo.completado).length;
  }

  RemoveCompleted() {
    this.store.dispatch(new RemoveAllCompletedTodoAction());
  }
}
