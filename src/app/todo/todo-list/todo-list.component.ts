import {Component, OnInit} from '@angular/core';
import {Todo} from '../model/todo.model';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../app.reducers';
import * as fromFilter from '../../filter/filter.action.js';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

  todos: Todo[] = [];
  filter: fromFilter.FilterValids;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.store.subscribe(state => {
      this.todos = state.todos;
      this.filter = state.filter;
    });
  }

}
