import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../app.reducers';
import {ToggleAllAction} from './todo.actions';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  toogleAll: FormControl;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {

    this.toogleAll = new FormControl('');

    this.toogleAll.valueChanges.subscribe(value => {
      this.store.dispatch(new ToggleAllAction(value));
    });
  }

}
