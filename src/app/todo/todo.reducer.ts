import * as fromTodo from './todo.actions';
import {Todo} from './model/todo.model';

let estadoInicial: Todo[] = [];
const todo1: Todo = new Todo('primeiro resultado');
const todo12: Todo = new Todo('segundo resultado');
estadoInicial = [...estadoInicial, todo1, todo12];


export function todoReducer(state = estadoInicial, action: fromTodo.Actions): Todo[] {
  switch (action.type) {

    case fromTodo.AGREGAR_TODO:
      const todo = new Todo(action.texto);
      return [...state, todo];

    case fromTodo.EDIT_TODO:
      return state.map(item => {
        if (item.id === action.id) {
          return {...item, texto: action.texto};
        } else {
          return item;
        }
      });


    case fromTodo.TOGGLE_TODO:
      return state.map(item => {
        if (item.id === action.id) {
          return {...item, completado: !item.completado};
        } else {
          return item;
        }
      });

    case fromTodo.REMOVE_TODO:
      return state.filter(item => item.id !== action.id);


    case fromTodo.TOGGLE_ALL_TODO:
      return state.map(item => {
        return {...item, completado: action.toggle};
      });

    case fromTodo.REMOVE_ALL_COMPLETED_TODO:
      return state.filter(item => !item.completado);


    default:
      return state;
  }


}
