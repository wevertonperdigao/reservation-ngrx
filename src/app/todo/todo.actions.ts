import {Action} from '@ngrx/store';

export const AGREGAR_TODO = '[TODO] Agregar todos';
export const TOGGLE_TODO = '[TODO] Toggle todo';
export const EDIT_TODO = '[TODO] Edit todo';
export const REMOVE_TODO = '[TODO] REMOVE todo';
export const TOGGLE_ALL_TODO = '[TODO] Toggle all todo';
export const REMOVE_ALL_COMPLETED_TODO = '[TODO] Remove all completed todo';


export class AgreagarTodoAction implements Action {
  readonly type = AGREGAR_TODO;

  constructor(public texto: string) {
  }

}

export class ToggleTodoAction implements Action {
  readonly type = TOGGLE_TODO;

  constructor(public id: number) {
  }
}

export class EditTodoAction implements Action {
  readonly type = EDIT_TODO;

  constructor(public texto: string, public id: number) {
  }
}

export class RemoveTodoAction implements Action {
  readonly type = REMOVE_TODO;

  constructor(public id: number) {
  }
}

export class ToggleAllAction implements Action {
  readonly type = TOGGLE_ALL_TODO;

  constructor(public toggle: boolean) {
  }
}

export class RemoveAllCompletedTodoAction implements Action {
  readonly type = REMOVE_ALL_COMPLETED_TODO;
}


export type Actions =
  AgreagarTodoAction
  | ToggleTodoAction
  | EditTodoAction
  | RemoveTodoAction
  | ToggleAllAction
  | RemoveAllCompletedTodoAction;
