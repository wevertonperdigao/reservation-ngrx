import {Component, ElementRef, Input, OnInit, ViewChild, ViewChildren} from '@angular/core';
import {Todo} from '../model/todo.model';
import {FormControl, Validators} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../app.reducers';
import {EditTodoAction, RemoveTodoAction, ToggleTodoAction} from '../todo.actions';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent implements OnInit {

  @Input() todo: Todo;
  @Input() index: number;
  checkbox: FormControl;
  descricao: FormControl;
  isEdit = false;
  @ViewChild('inputdescricao', {static: false}) Inputdescricao: ElementRef;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {


    this.checkbox = new FormControl(this.todo.completado);
    this.descricao = new FormControl(this.todo.texto, Validators.required);

    this.checkbox.valueChanges.subscribe(value => this.store.dispatch(new ToggleTodoAction(this.todo.id)));
  }

  editar() {
    setTimeout(() => {
      this.Inputdescricao.nativeElement.focus();
    }, 1);

  }


  saveEdit() {
    if (this.descricao.valid) {
      this.store.dispatch(new EditTodoAction(this.descricao.value, this.todo.id));
    } else {
      this.descricao.setValue(this.todo.texto);
    }

  }

  remover() {
    this.store.dispatch(new RemoveTodoAction(this.todo.id));
  }
}
